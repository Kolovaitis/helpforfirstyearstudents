package by.kolovaitis;

import by.kolovaitis.sorters.*;

import java.util.Arrays;
import java.util.Random;

public class Main {
private static final int MAX_ARRAY_VALUE = 100;
private static final int ARRAY_SIZE = 10;

    public static void main(String[] args) {
        int[] startArray = getRandomArray(ARRAY_SIZE);
        printArray(startArray);
        Sorter sorter = new QuickSorter();
        sorter.sort(startArray);
        printArray(startArray);
//       System.out.println(testSorter(sorter, startArray));
    }

    private static int[] getRandomArray(int length) {
        Random random = new Random();
        int[] result = new int[length];
        for (int i = 0; i < length; i++) {
            result[i] = random.nextInt(MAX_ARRAY_VALUE);
        }
        return result;
    }
    private static void printArray(int[] array){
        System.out.println(Arrays.toString(array));
    }

    private static long testSorter(Sorter sorter, int[] startArray){
        int[] array = Arrays.copyOf(startArray, startArray.length);
        long start = System.currentTimeMillis();
        sorter.sort(array);
        long end = System.currentTimeMillis();
        return end-start;
    }

}
