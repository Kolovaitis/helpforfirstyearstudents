package by.kolovaitis.sorters;

public class MergeSorter extends Sorter {
    @Override
    public void sort(int[] array) {
        if (array.length < 2) {
            return;
        }

        int[] arrayA = new int[array.length / 2];
        System.arraycopy(array, 0, arrayA, 0, array.length / 2);

        int[] arrayB = new int[array.length - array.length / 2];
        System.arraycopy(array, array.length / 2, arrayB, 0, array.length - array.length / 2);

        sort(arrayA);
        sort(arrayB);
        mergeArrays(arrayA, arrayB, array);
    }

    public int[] mergeArrays(int[] arrayA, int[] arrayB, int[]resultArray) {
        int lengthA = arrayA.length;
        int lengthB = arrayB.length;

        int positionA = 0;
        int positionB = 0;

        for (int i = 0; i < resultArray.length; i++) {
            if (positionA == lengthA) {
                resultArray[i] = arrayB[positionB];
                positionB++;
            } else if (positionB == lengthB) {
                resultArray[i] = arrayA[positionA];
                positionA++;
            } else if (arrayA[positionA] > arrayB[positionB]) {
                resultArray[i] = arrayB[positionB];
                positionB++;
            } else {
                resultArray[i] = arrayA[positionA];
                positionA++;
            }
        }
        return resultArray;
    }
}
