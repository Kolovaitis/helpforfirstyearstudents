package by.kolovaitis.sorters;

public class QuickSorter extends Sorter {

    @Override
    public void sort(int[] array) {
        sortRegion(array, 0, array.length - 1);
    }

    private void sortRegion(int[] array, int low, int high) {

        int middle = low + (high - low) / 2;
        int middleElement = array[middle];

        int i = low, j = high;
        while (i <= j) {
            while (array[i] < middleElement) {
                i++;
            }

            while (array[j] > middleElement) {
                j--;
            }

            if (i <= j) {//меняем местами
                swap(array, i, j);
                i++;
                j--;
            }
        }

        if (low < j)
            sortRegion(array, low, j);

        if (high > i)
            sortRegion(array, i, high);
    }
}

