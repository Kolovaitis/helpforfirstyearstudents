package by.kolovaitis.sorters;

public class BubbleSorter extends Sorter {
    @Override
    public void sort(int[] array) {
        int n = array.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1] > array[j]) {
                    swap(array, j, j - 1);
                }

            }

        }
    }
}
