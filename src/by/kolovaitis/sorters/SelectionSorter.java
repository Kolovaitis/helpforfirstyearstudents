package by.kolovaitis.sorters;

public class SelectionSorter extends Sorter {
    @Override
    public void sort(int[] array) {
        for (int i = 0; i < array.length-1; i++) {
            int minInd = i;
            for (int j = i+1; j < array.length; j++) {
                if (array[j] < array[minInd]) {
                    minInd = j;
                }
            }
            swap(array, i, minInd);
        }
    }
}
