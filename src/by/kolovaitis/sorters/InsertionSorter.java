package by.kolovaitis.sorters;

public class InsertionSorter extends Sorter {

    @Override
    public void sort(int[] array) {
        int n = array.length;
        for (int i = 1; i < n; i++) {
            int value = array[i];
            int j = i - 1;
            while ((j > -1) && (array[j] > value)) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = value;
        }
    }
}
